DASHTEST - dashmote test App
============================

Install
-------

    git clone https://Lokky@bitbucket.org/Lokky/dashtest.git
    git submodule update --init
    npm install

Run
---

    npm run make
    npm start

dashmote's test task
--------------------

#### Goals:
* Create image grid layout.
* Grid has 50 images.
* Every image is a rectangle with width = 300 and random height (from 150 to 600)
* Images should be loaded from the mockup JSON provided

#### Requirements:
* Click on the image should delete it from the layout.
* Scroll to bottom should add more results.
* Also we have fixed information block on the page, which shows current amount of images in the grid.
* Should be accessible through Server url, or running locally 
* Code should be shipped as well

#### Nice to have ;):
* Using of mock promise to get the image data is a plus, but not neccessary.
* Using of Angular is a big plus, but not neccesary.
* Using of ES6 syntax is a huge plus but not neccessary.
* Writing of couple of unit-tests is a fantastic plus, but not neccessary.

#### Link to JSON
    https://api.myjson.com/bins/29ja6